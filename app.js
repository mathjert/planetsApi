const express = require('express');
const app = express();
const port = process.env.PORT || 5000;
const { db } = require('./db');
const { Auth } = require('./auth');
const cors = require('cors');
const publicPath = [
    '/api/v1/auth/generate-key'
]

const auth = new Auth(db.getPool());

app.use(cors());
app.use(express.json())
    .use(express.urlencoded({
        extended: false
    }))
    .use(async (request, response, next) => {
        try {
            if(publicPath.includes(request.originalUrl)){
               return next();
            }
            const authResult = await auth.authenticateKey(request, response);

        if(authResult === true){
            next();
        }else{
            response.status(401).json(authResult);
        }
        } catch (err) {
            response.json({
                error: err.message
            });
        }
        
    })
    .get('/api/v1/planets', (request, response) => db.getPlanets(request, response))
    .post('/api/v1/planets/add', (request, response) => db.addPlanet(request, response))
    .patch('/api/v1/planets/update', (request, response) => db.updatePlanet(request, response))
    .delete('/api/v1/planets/delete', (request, response)=> db.deletePlanet(request, response))
    .post('/api/v1/auth/generate-key', (request, response) => auth.generateKey(request, response))
    .listen(port, () => { console.log(`server has connected to port ${port}`); })


    /* 
     * Heroku url: https://planetsapi-noroff.herokuapp.com 
     */