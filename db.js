const { Pool } = require('pg');
require('dotenv').config();
class Db {
    constructor() {
        this.config = {
            user: process.env.DB_USER,
            host: process.env.DB_HOST,
            database: process.env.DB_DATABASE,
            password: process.env.DB_PASSWORD,
            port: process.env.DB_PORT
        };
        this.pool = new Pool(this.config);
    }

    getPool() {
        return this.pool;
    }

    getPlanets(request, response) {
        this.pool.query('SELECT * FROM planet', (err, results) => {
            if (err) {
                response.status(500).json({
                    success: false,
                    message: err.message
                });
            }
            response.status(200).json(results.rows); // sucess: true, planets: result.rows
        });
    }

    async addPlanet(request, response) {
        let { name, size, date_discovered } = request.body.planet;
        // check that all are set
        if (!name || !size || !date_discovered) {
            return response.json({
                success: false,
                error: 'Set all attributes: name, size, date_discovered'
            });
        }
        try {
            const checkQuery = await this.pool.query('SELECT id FROM planet where name = $1', [name]);

            if (checkQuery.rowCount > 0) {
                return response.status(400).json({
                    success: false,
                    message: 'Item already exist'
                });
            }

            const insertQuery = await this.pool.query('INSERT INTO planet(name, size, date_discovered) VALUES($1, $2, $3) RETURNING id, name, size, date_discovered',
                [name, size, date_discovered]);

            response.status(201).json({
                success: true,
                planet: insertQuery.rows, // rows[0]
                message: 'Planet inserted'
            });



        } catch (err) {
            response.status(500).json({
                success: false,
                message: err.message
            });
        }
    }

    /* 
     * Only id is required. Rest is optional. If you are not updating an column do not incude column in
     * the json object passed to the function. Example that will update name and size but not date_discovered:
        {
         "planet": {
    	        "id": 14,
                "name": "11 Ursae Minoris a",
                "size": "14,74 Jupiters",
            }
        }
     * The assignment in the newPlanet object is ordered in the order of importance. The name given from
     * the request has highest priority, however it it is not given we will take the name from the query
     * where we check if the object does exist.
     */
    async updatePlanet(request, response) {
        let { id, name, size, date_discovered } = request.body.planet;
        let newPlanet;

        if (!id) {
            return response.status(400).json({
                success: false,
                message: "No id given"
            });
        }
        try {
            const checkQuery = await this.pool.query('SELECT * FROM planet WHERE id = $1', [id]);
            if (checkQuery.rowCount > 0) {

                newPlanet = {
                    id: id,
                    name: name || checkQuery.rows[0].name,
                    size: size || checkQuery.rows[0].size,
                    date_discovered: date_discovered || checkQuery.rows[0].date_discovered,
                };
            } else {
                response.status(404).json({
                    success: false,
                    message: "no such planet"
                });
                return;
            }

            const updateQuery = await this.pool.query('UPDATE planet SET name = $1, size = $2, date_discovered = $3 WHERE id = $4',
                [newPlanet.name, newPlanet.size, newPlanet.date_discovered, newPlanet.id]);

            response.status(200).json({
                success: true,
                planet: newPlanet,
                message: "Planet is updated",
            });

        } catch (err) {
            response.status(500).json({
                success: false,
                message: err.message,
            });
        }
    }

    async deletePlanet(request, response) {
        let { id } = request.query;

        try {
            const deleteQuery = await this.pool.query('DELETE FROM planet WHERE id = $1', [id]);
            if (deleteQuery.rowCount > 0) {
                response.status(200).json({
                    success: true,
                    message: "Deleted item",
                });
            } else {
                response.status(404).json({
                    success: false,
                    message: "Item does not exist",
                });
            }

        } catch (err) {
            console.log(err);
            response.status(500).json({
                success: false,
                message: err.message,
            });
        }


    }
}

module.exports.db = new Db();