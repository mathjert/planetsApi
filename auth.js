const crypto = require('crypto');
class Auth{
    
    constructor(pool){
        this.pool = pool;
    }

    async generateKey(request, response){
        try {
            const apiKey = await crypto.randomBytes(32).toString('hex');
            const result = await this.pool.query('INSERT INTO api_key (key) VALUES ($1) RETURNING id', [apiKey]);

            if(result.rowCount > 0){
                response.status(201).json({ 
                    apiKey
                });
            }else{
                response.status(500).json({
                    success: false,
                    error: 'Error on server side'
                })
            }
        } catch (err) {

            response.status(500).json({
                success: false,
                error: err.message
            })
        }
    }
    async authenticateKey(request){
        try {
            const { authorization } = request.headers;

            
            if(!authorization){
                return {
                    error: 'No API key found.'
                };
            }
            
            const key = authorization.split(' ')[1];
            const keyResult = await this.pool.query('SELECT * FROM api_key WHERE key = $1 AND active = 1', [key]);
             
            return keyResult.rowCount > 0 ? true :  {error: 'Invalid api key'};

        } catch (err) {
            return {
                error: err.message
            };
        }
    }
}

module.exports.Auth = Auth;